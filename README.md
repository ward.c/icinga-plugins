# Icinga Plugins

My Collection of icinga plugins:

 - check_wireguard.pl: [https://gitlab.com/alasdairkeyes/nagios-plugin-check_wireguard](https://gitlab.com/alasdairkeyes/nagios-plugin-check_wireguard)
 - check_docker.py: [https://github.com/timdaman/check_docker/](https://github.com/timdaman/check_docker/)